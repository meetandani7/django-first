from django.shortcuts import render
from .models import Destination


def index(request):
	# dest1=Destination("Delhi","This is good city.","7000","destination_1.jpg",False)
	# dest2=Destination("Mumbai","It is known as Film city","6070","destination_2.jpg",True)
	# dest3=Destination("Rajkot","It is very rangilu.","6000","destination_3.jpg",False)
	# dests=[dest1,dest2,dest3]
	dests	=Destination.objects.all()
	return render(request,'index.html',{"dests":dests})


