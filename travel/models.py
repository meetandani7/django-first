from django.db import models

# class Destination(object):
# 	"""docstring for Destination"""

# 	def __init__(self, name,desc,price,image,offer):
# 		super(Destination, self).__init__()
# 		self.name = name
# 		self.desc = desc
# 		self.price = price
# 		self.image = image
# 		self.offer = offer		
		
class Destination(models.Model):

	name = models.CharField(max_length=200)
	image = models.ImageField(upload_to='pics')
	desc = models.TextField()
	price = models.IntegerField()
	offer = models.BooleanField(default=False)

		
		