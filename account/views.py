from django.shortcuts import render,redirect
from django.contrib.auth.models import auth , User
from django.contrib import messages

def login(request):
	if request.method=="POST":
		print("hey")
		user_name=request.POST['user_name']
		pwd=request.POST['password']

		user=auth.authenticate(username=user_name,password=pwd)

		if user is not None:
			auth.login(request,user)
			return redirect('/travel')
		else:
			messages.info(request,'invalid credential')
			return redirect('login')
		
	else:
		return render(request,'login.html')

def registration(request):
	if request.method == 'POST':
		first_name=request.POST['first_name']
		last_name=request.POST['last_name']
		user_name=request.POST['user_name']
		email=request.POST['email']
		pwd1=request.POST['password1']
		pwd2=request.POST['password2']
        #if (first_name==" " and last_name==" " and user_name==" " and email==" " and pwd1==" " and pwd2==" "):
        

		if pwd2==pwd1:
			if User.objects.filter(username=user_name).exists():
				messages.info(request,'username taken')
				return redirect('registration')
			elif User.objects.filter(email=email).exists():
				messages.info(request,'email taken')
				return redirect('registration')
			else:
				user=User.objects.create_user(username=user_name,password=pwd1,email=email,first_name=first_name,last_name=last_name)
				user.save()
				messages.info(request,'username taken')
				return redirect('login')
		else:
			messages.info(request,'password are not matching')
			
			return redirect('registration')
		return redirect('/travel')

	else:
		return render(request,'register.html')

def logout(request):
	auth.logout(request)
	return redirect('/travel')
